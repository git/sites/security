require_relative 'glsav1.rb'
require 'date'

class GLSARepository
  include Singleton

  CACHE_SECONDS = 3600
  DATA_PATH     = File.join(File.dirname(__FILE__), '../data/glsa/')

  def initialize
    update!
  end

  def update!
    advisories = {}

    Dir.glob(DATA_PATH + '*.xml').each do |glsa_file|
      f = File.open(glsa_file)
      glsa = Nokogiri::XML(f)
      f.close

      if glsa.root.key? 'version'
        # Future extension: GLSAv2
      else
        begin
          advisories[glsa.root['id']] = GLSAv1.new.parse(glsa)
        rescue
          next
        end
      end
    end

    @load_date = DateTime.now
    @advisories = advisories.freeze
    @latest = advisories.keys.sort.reverse.freeze
  end

  def get
    @advisories
  end

  def latest_ids
    @latest
  end

  def latest(n = 10)
    @latest[0...n].map { |id| @advisories[id] }
  end

  def[](id)
    @advisories[id]
  end

  def has?(id)
    @advisories.key? id
  end

  private
  
  def update?
    if ((DateTime.now - @load_date) * 60 * 60 * 24).to_i > CACHE_SECONDS
      update!
    end
  end
end
