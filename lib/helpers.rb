require 'sanitize'
require 'erb'
require 'rss'

COMP_MAP = {
  '>=' => 'ge',
  '>'  => 'gt',
  '='  => 'eq',
  '<=' => 'le',
  '<'  => 'lt',
  'revision <' => 'rlt',
  'revision <=' => 'rle',
  'revision >' => 'rgt',
  'revision >=' => 'rge'
}.freeze

helpers do
  def h(text)
    Rack::Utils.escape_html(text)
  end

  def u(text)
    ERB::Util.url_encode(text)
  end

  def h2(text)
    Sanitize.clean(text, Sanitize::Config::BASIC)
  end

  def code2pre(text)
    text.gsub('<code>', '<pre>').gsub('</code>', '</pre>').gsub(/ +/, ' ').chomp
  end

  def date_format(d)
    d.strftime('%B %d, %Y')
  end

  # Returns the comparator in the format needed for the XML
  def xml_comp(val)
    COMP_MAP[val]
  end

  def reverse_xml_comp(val)
    COMP_MAP.invert[val]
  end

  def render_version(ver)
    vs = ver[1].split ':'

    if vs.size == 1
      '%s <strong>%s</strong>' % [h(reverse_xml_comp(ver[0])), h(ver[1])]
    else
      '%s <strong>%s</strong> in slot <strong>%s</strong>' % [h(reverse_xml_comp(ver[0])), h(vs[0]), h(vs[1])]
    end
  end

  def yaml_dump(items)
    items.map do |item|
      {
        'id' => item.id,
        'title' => item.title,
        'severity' => item.severity,
        'uri' => BASE_URL + 'glsa/' + item.id
      }
    end.to_yaml
  end

  def feed(type, items)
    RSS::Maker.make(type) do |maker|
      maker.channel.author      = 'Gentoo security team'
      maker.channel.about       = 'https://security.gentoo.org/glsa'
      maker.channel.link        = 'https://security.gentoo.org/glsa'
      maker.channel.description = 'This feed contains new Gentoo Linux security advisories. Contact security@gentoo.org with questions.'
      maker.channel.title       = 'Gentoo Linux security advisories'
      maker.channel.updated     = items.first.revised.to_s

      items.each do |input_item|
        maker.items.new_item do |item|
          item.link = BASE_URL + 'glsa/' + input_item.id
          item.title = 'GLSA %s: %s' % [input_item.id, input_item.title]
          item.updated = input_item.revised.to_s
        end
      end
    end.to_s
  end
end
