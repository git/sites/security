FROM ruby:3.0

RUN apt update
RUN apt install -y make gcc g++ ruby-dev ruby-bundler git thin

COPY . /var/www/security

WORKDIR /var/www/security/data

RUN git clone https://anongit.gentoo.org/git/data/glsa.git

EXPOSE 9995

WORKDIR /var/www/security

RUN bundle install --deployment

CMD bundle exec thin -p 9995 start
