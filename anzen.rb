# /usr/bin/env ruby
# 安全 [anzen] aka security.gentoo.org
# Alex Legler <a3li@gentoo.org>
# AGPLv3

require 'bundler/setup'
require 'yaml'
require 'singleton'
require 'nokogiri'
require 'sinatra'
require 'sinatra/partial'

require_relative 'lib/helpers.rb'
require_relative 'lib/glsa_repository.rb'

configure do
  set :partial_template_engine, :erb
  mime_type :atom, 'application/atom+xml'
end

# Implicitly load advisories
GLSARepository.instance

BASE_URL = 'https://security.gentoo.org/'.freeze

get '/glsa/?' do
  @ids = GLSARepository.instance.latest_ids
  @nav = :glsa
  erb :glsa
end

get '/glsa/feed.:format' do
  items = GLSARepository.instance.latest 50
  case params[:format]
  when 'atom'
    content_type :atom
    feed('atom', items)
  when 'rss1'
    content_type :xml
    feed('1.0', items)
  when 'rss', 'rss2'
    content_type :xml
    feed('2.0', items)
  when 'yaml'
    content_type :yaml
    yaml_dump(items)
  else
    status 404
    body 'Feed not available.'
    return
  end
end

get '/glsa/:glsaid.xml' do
  if params[:glsaid] =~ /^\d{6}-\d{2}$/ && GLSARepository.instance.has?(params[:glsaid])
    send_file(File.join(File.dirname(__FILE__), 'data/glsa/glsa-' + params[:glsaid] + '.xml'), type: :xml)
  else
    status 404
    body 'GLSA not found.'
    return
  end
end

get '/glsa/:glsaid' do
  if GLSARepository.instance.has? params[:glsaid]
    if params[:passthru] == '1'
      redirect "/glsa/#{params[:glsaid]}.xml"
      return
    end

    @glsa = GLSARepository.instance[params[:glsaid]]
    template = :glsa

    if @glsa.is_a? GLSAv1
      template = :'glsa/glsav1'
      @title = "#{@glsa.title} (GLSA #{@glsa.id})"
      @description = "#{@glsa.synopsis}"
    end

    @nav = :glsa
    erb template
  else
    status 404
    body 'GLSA not found.'
    return
  end
end

get '/subscribe' do
  @nav = :subscribe
  erb :subscribe
end

get '/' do
  @ids = GLSARepository.instance.latest_ids
  @nav = :index
  erb :index
end

get '/update' do
  GLSARepository.instance.update!
  'ok'
end
